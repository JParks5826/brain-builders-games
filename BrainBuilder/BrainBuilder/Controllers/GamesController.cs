﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BrainBuilder.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

/*
 ********
 * Games Controller
 * Controller used for games
 * Date Created: 04/092021
 * Desert Sands 
 *********
*/


namespace BrainBuilder.Controllers
{
    public class GamesController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly BrainBuilderDBContext _context;


        public GamesController(RoleManager<IdentityRole> roleManager,
                                UserManager<IdentityUser> userManager,
                                BrainBuilderDBContext context)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            _context = context;

        }

        [BindProperty]
        public InputModel Input { get; set; }

        public partial class InputModel
        {
            public int FinalScore { get; set; }

            public int MovesTaken { get; set; }

            public int TimeTaken { get; set; }

            public string MathInput { get; set; }

        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            //Checks if user has an active subscription - changes the lists of the game if it's active
            if (isSubscriptionActive())
            {
                //Lists through game information - even membership exclusive games
                var games = _context.Games.OrderBy(g => g.GameId);

                //Checks if user has an active subscription
                ViewData["isActive"] = isSubscriptionActive();

                return View(await games.ToListAsync());
            }
            else
            {
                //Lists through game information - no membership exclusive games
                var games = _context.Games.OrderBy(g => g.GameId).Where(g => g.IsMembershipExclusive == false);

                //Checks if user has an active subscription
                ViewData["isActive"] = isSubscriptionActive();

                return View(await games.ToListAsync());
            }
        }

        // GET: Games/Details/5
        public IActionResult Details(int id)
        {
            //For now, the game will redirect hardcoded
            if (id == 1)
            {
                return RedirectToAction("MatchingGame");
            }
            else if (id == 2)
            {
                return RedirectToAction("MathGame");
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Matching Game
        /// </summary>
        /// <returns></returns>
        public IActionResult MatchingGame()
        {
            InputModel inputModel = new InputModel();

            return View(inputModel);
        }

        /// <summary>
        /// Math Game
        /// </summary>
        /// <returns></returns>
        public IActionResult MathGame()
        {
            InputModel inputModel = new InputModel();

            //Checks if user has an active subscription
            ViewData["isActive"] = isSubscriptionActive();

            return View(inputModel);
        }

        /// <summary>
        /// Submits stats to matching table
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<IActionResult> Submit_Matching(InputModel input)
        {
            //Checks if user is authenticated
            if (User.Identity.IsAuthenticated)
            {
                //Gets id of current user
                Accounts account = _context.Accounts.Where(p => p.Username == User.Identity.Name).FirstOrDefault();

                //Gets information of the correct person
                var profile = await _context.Profiles
                    .Include(p => p.ProvinceCodeNavigation)
                    .FirstOrDefaultAsync(p => p.Id == account.AccountId);

                //Adds stat to database
                var gameStats = new GameStats
                {
                    //Id = account.AccountId,
                    AccountId = account.AccountId,
                    GameId = 1,
                };

                _context.Add(gameStats);
                await _context.SaveChangesAsync();

                //Adds matching stats to database
                var matchingGameStats = new GameStatsMatching
                {
                    Id = gameStats.Id,
                    AccountId = account.AccountId,
                    FinalScore = input.FinalScore,
                    MovesTaken = input.MovesTaken,
                    TimeTaken = input.TimeTaken,
                    Date = DateTime.Now
                };

                _context.Add(matchingGameStats);
                await _context.SaveChangesAsync();

                TempData["Message"] = "Game stat added successfully.";
                //StatusMessage = "An error has occured, please try again.";
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Games");
            }
            else
            {
                return RedirectToAction("Index", "Games");
            }
        }

        /// <summary>
        /// Submits stats to math stats table
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<IActionResult> Submit_Math(InputModel input)
        {
            //Checks if user is authenticated
            if (User.Identity.IsAuthenticated)
            {
                //Gets id of current user
                Accounts account = _context.Accounts.Where(p => p.Username == User.Identity.Name).FirstOrDefault();

                //Gets information of the correct person
                var profile = await _context.Profiles
                    .Include(p => p.ProvinceCodeNavigation)
                    .FirstOrDefaultAsync(p => p.Id == account.AccountId);

                //Adds stat to database
                var gameStats = new GameStats
                {
                    //Id = account.AccountId,
                    AccountId = account.AccountId,
                    GameId = 2,
                };

                _context.Add(gameStats);
                await _context.SaveChangesAsync();

                //Splits the input 
                string[] results = input.MathInput.Split(" out of ");

                //Checks for nulls - if nulls, it will display as 0
                for (int i = 0; i < results.Length; i++)
                {
                    if (results[i] == null || results[i] == "")
                    {
                        results[i] = "0";
                    }
                }

                int questionAnswered = Convert.ToInt16(results[0]);
                int correctAnswers = Convert.ToInt16(results[1]);

                //Adds math stats to database
                var mathGameStats = new GameStatsMath
                {
                    Id = gameStats.Id,
                    AccountId = account.AccountId,
                    QuestionsAnswered = questionAnswered,
                    CorrectAnswers = correctAnswers,
                    Date = DateTime.Now
                };

                _context.Add(mathGameStats);
                await _context.SaveChangesAsync();

                TempData["Message"] = "Game stat added successfully.";
                //StatusMessage = "An error has occured, please try again.";
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Games");
            }
            else
            {
                return RedirectToAction("Index", "Games");
            }
        }

        //Checks if user is authenticated
        public bool isSubscriptionActive()
        {
            bool isActive = false;

            //Checks if user is authenticated and they already have a paid membership
            if (User.Identity.IsAuthenticated)
            {
                //Gets id of current user
                Accounts account = _context.Accounts.Where(p => p.Username == User.Identity.Name).FirstOrDefault();

                //Checks if there any subscription on record
                if (_context.UserSubscriptions.Any(u => u.AccountId == account.AccountId))
                {
                    //Checks if the subscription is active
                    UserSubscriptions userSubscriptions = _context.UserSubscriptions.Where(u => u.AccountId == account.AccountId).FirstOrDefault();

                    if (userSubscriptions.IsActive)
                    {
                        isActive = true;
                    }
                    else
                    {
                        isActive = false;
                    }
                }
                else
                {
                    isActive = false;
                }
            }
            else
            {
                isActive = false;
            }

            return isActive;
        }
    }
}
