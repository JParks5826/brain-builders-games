﻿using System;
using System.Collections.Generic;

namespace BrainBuilder.Models
{
    public partial class GameStatsMath
    {
        public int Id { get; set; }
        public int? AccountId { get; set; }
        public int? QuestionsAnswered { get; set; }
        public int? CorrectAnswers { get; set; }
        public DateTime? Date { get; set; }

        public virtual Accounts Account { get; set; }
        public virtual GameStats IdNavigation { get; set; }
    }
}
